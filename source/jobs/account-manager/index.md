---
layout: markdown_page
title: "Account Manager Responsibilities and Tasks"
---


## Responsibilities

* Manage relationships with prospective and current customers.
   * Work with customers, engaging our [service engineers](https://about.gitlab.com/jobs/service-engineer) as appropriate.
* Manage new and grow existing business with customers.
* Contribute to post-mortem analysis on wins/losses.
   * Communicate lessons learned to the team, including account managers, the marketing team, and the technical team.
* Manage and report on part of the sales pipeline.
* Keep the sales process smooth and robust, using tools such as SalesForce.
* Document sales and account management processes.
* Additional responsibilities for Senior Account Managers:
   * Take the lead in large deals
   * Coach and train other Account Managers